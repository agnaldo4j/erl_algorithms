%%%-------------------------------------------------------------------
%%% @author agnaldo4j
%%% @copyright (C) 2017, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 24. Jul 2017 9:37 PM
%%%-------------------------------------------------------------------
-module(union_find_api).
-author("agnaldo4j").
-behavior(gen_server).

%% API
-export([start_link/0]).
-export([stop/0]).
-export([init/1]).

-export([handle_call/3]).
-export([handle_cast/2]).
-export([terminate/2]).
-export([code_change/3]).

-spec start_link() -> {ok, pid()}.
start_link() ->
  gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

-spec stop() -> stopped.
stop() ->
  gen_server:call(?MODULE, stop).

setup()->
  gen_server:call(?MODULE, stop).

union() ->
  gen_server:call(?MODULE, stop).

find() ->
  gen_server:call(?MODULE, stop).

connect() ->
  gen_server:call(?MODULE, stop).

count() ->
  gen_server:call(?MODULE, stop).

%% INTERNAL API
-spec init([]) -> {ok, #{}}.
init([]) ->
  {ok, #{}}.

-type from() :: {pid(), term()}.
-spec handle_call
    (stop, from(), State) -> {stop, normal, stopped, State}
  when State::#{}.
handle_call(stop, _From, State) ->
  {stop, normal, stopped, State}.

-spec handle_cast(_, State) -> {noreply, State} when State::#{}.
handle_cast(_Msg, State) ->
  {noreply, State}.

-spec terminate(_, _) -> ok.
terminate(_Reason, _State) ->
  ok.

-spec code_change(_, State, _) -> {ok, State} when State::#{}.
code_change(_OldVsn, State, _Extra) ->
  {ok, State}.
